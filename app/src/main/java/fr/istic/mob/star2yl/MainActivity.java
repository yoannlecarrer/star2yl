package fr.istic.mob.star2yl;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import fr.istic.mob.star2yl.ui.main.fragment.Fragment1;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText editTextDate;
    private Button buttonDate;
    private CheckBox checkBoxIsSpinnerMode;

    private int lastSelectedYear;
    private int lastSelectedMonth;
    private int lastSelectedDayOfMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, Fragment1.newInstance("test","test"))
                    .commitNow();
        }
    }
}