package fr.istic.mob.star2yl.ui.main.models;

public class Stop {
    private String stop_code ;

    private String stop_name;
    private String stop_desc;
    private String stop_lat;
    private String stop_lon;
    private String wheelchair_boarding;

    public Stop(String stop_code, String stop_name, String stop_desc, String stop_lat, String stop_lon, String wheelchair_boarding){
        this.stop_code = stop_code;
        this.stop_name = stop_name;
        this.stop_desc = stop_desc;
        this.stop_lat = stop_lat;
        this.stop_lon = stop_lon;
        this.wheelchair_boarding = wheelchair_boarding;

    }

    // --- GETTER ---

    public String getStop_code() { return stop_code; }
    public String getStop_name() { return stop_name; }
    public String getStop_desc() { return stop_desc; }
    public String getStop_lat() { return stop_lat; }
    public String getStop_lon() { return stop_lon; }
    public String getWheelchair_boarding() { return wheelchair_boarding; }

    // --- SETTER ---

    public void setStop_code(String stop_code) { this.stop_code = stop_code; }
    public void setStop_name(String stop_name) { this.stop_name = stop_name; }
    public void setStop_desc(String stop_desc) { this.stop_desc = stop_desc; }
    public void setStop_lat(String stop_lat) { this.stop_lat = stop_lat; }
    public void setStop_lon(String stop_lon) { this.stop_lon = stop_lon; }
    public void setWheelchair_boarding(String wheelchair_boarding) { this.wheelchair_boarding = wheelchair_boarding; }
}
